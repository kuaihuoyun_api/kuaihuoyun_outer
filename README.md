# 文档已迁移至：http://gogs.kuaihuoyun.com:3000/gongmingwei/kuaihuoyun_outer

TMS外链接口文档

## 目录
- [简介](#简介)
- [对接方式](#对接方式)
- [接口详细信息](#接口详细信息)
  - [登陆接口](#登陆接口)
  - [受理单智能追踪接口](#受理单智能追踪接口)


## 简介

用于将TMS系统中的部分功能集成在外部系统

## 对接方式

首先调用登陆接口进行登录，从cookie中获取ttms的值，此值为token，之后再调用其他接口。接口的请求地址为系统域名：
- **测试环境**：http://go3tms.test.56ctms.com
- **正式环境**：http://go3tms.kuaihuoyun.com
- **独立部署用户请使用自己的域名**

请求示例：http://go3tms.test.56ctms.com/ttms/account/outerLogin?phone=19012345678&password=123456


## 接口详细信息

### 登陆接口

**简要描述：** 用于登陆系统，获取token，token在cookie中，key为`ttms`

**请求 URL：** `/ttms/account/outerLogin`

**请求方式：** POST

**请求参数：**

| 参数名 | 必选 | 类型 | 说明 |
|:----:|:---:|:-----:|:-----:|
| `phone` | 是 | `String` | `账号` |
| `password` | 是 | `String` | `密码` |

**返回示例**

- 调用成功示例

```java
{
    "status": 200,
    "body": "Success!",
    "message": null
}
```
- 调用失败示例

```java
{
    "message": "密码不符",
    "status": 500
}
{
    "message": "账号无效",
    "status": 500
}
```

### 受理单智能追踪接口

**简要描述：** 获取受理单智能追踪页面

**请求 URL：** `/ttms/jumpToOmsTracker`

**请求方式：** POST

**请求参数：**

| 参数名 | 必选 | 类型 | 说明 |
|:----:|:---:|:-----:|:-----:|
| `customerOrderNumber` | 是 | `String` | `受理单号` |
| `ttms` | 是 | `String` | `token，通过登录接口获取，有效期15天` |

**返回示例**

- 调用成功示例

<div align="center">
<img src="img/img_customer_order_tracker_success.png" width = "80%" height = "80%" alt="调用成功示例"/>
</div>

- 调用失败示例

<div align="center">
<img src="img/img_customer_order_tracker_error.png" width = "80%" height = "80%" alt="调用失败示例"/>
</div>